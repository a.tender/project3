#!/usr/bin/env python3

for num in reversed(range(21)):
    print(num, end=" ")
